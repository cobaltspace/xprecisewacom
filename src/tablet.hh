#ifndef XPRECISEWACOM_TABLET_HH
#define XPRECISEWACOM_TABLET_HH

#include <fmt/core.h>
#include <gdkmm/rectangle.h>
#include <glibmm/spawn.h>

void reset_area (const int id) {
  Glib::spawn_command_line_sync (
      fmt::format ("xsetwacom set {} ResetArea", id));
}

void set_area (const int id, const int w, const int h) {
  reset_area (id);
  std::string cmdout;
  Glib::spawn_command_line_sync (fmt::format ("xsetwacom get {} Area", id),
                                 &cmdout);
  int               tabletw, tableth;
  std::stringstream is (cmdout);
  is >> tabletw >> tableth >> tabletw >> tableth;
  if (w * tableth != h * tabletw) {
    int x = 0;
    int y = 0;
    if (w * tableth > h * tabletw) {
      int d   = h * tabletw / w;
      y       = (tableth - d) / 2;
      tableth = y + d;
    } else {
      int d   = w * tableth / h;
      x       = (tabletw - d) / 2;
      tabletw = x + d;
    }
    Glib::spawn_command_line_async (fmt::format (
        "xsetwacom set {} Area {} {} {} {}", id, x, y, tabletw, tableth));
  }
}

inline void map (const int id, const std::string_view output) {
  Glib::spawn_command_line_async (
      fmt::format ("xsetwacom set {} MapToOutput {}", id, output));
}

inline void map (const int id, const int w, const int h, const int x,
                 const int y) {
  map (id, fmt::format ("{}x{}+{}+{}", w, h, x, y));
}

inline void map (const int id, const Gdk::Rectangle& geometry) {
  map (id, geometry.get_width (), geometry.get_height (), geometry.get_x (),
       geometry.get_y ());
}

#endif /* XPRECISEWACOM_TABLET_HH */
