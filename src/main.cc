#include "tablet.hh"

#include <gdkmm/display.h>
#include <gdkmm/monitor.h>
#include <gdkmm/rectangle.h>
#include <gdkmm/seat.h>
#include <gtkmm/application.h>
#include <iostream>
#include <vector>

void usage (const char argv[]) {
  std::cout
      << "Usage: " << argv
      << " <command>\n"
         "Commands:\n"
         " a - Map tablet to all monitors\n"
         " m - Map tablet to a single monitor\n"
         " p <width> <height> [scale] - Map tablet to the part of\n"
         "  the monitor where your cursor is based on screen dpi\n"
         "   Width and Height: the width and height of your tablet in mm\n"
         "   Scale: precision multiplier\n";
}

int main (int argc, char* argv[]) {
  if (argc > 1) {
    std::vector<int> pointers;
    std::string      line;
    Glib::spawn_command_line_sync ("xsetwacom list", &line);
    std::stringstream is (line);
    while (std::getline (is, line)) {
      if (line.ends_with ("STYLUS    ") || line.ends_with ("ERASER    ")) {
        pointers.push_back (std::stoi (line.substr (1 + line.find (':'))));
      }
    }
    is.clear ();
    auto app = Gtk::Application::create ();
    switch (argv[1][0]) {
    case 'a': { // all monitors
      auto screen = Gdk::Display::get_default ()->get_default_screen ();
      std::cout << "a\n";
      int w = screen->get_width ();
      int h = screen->get_height ();
      for (const int pointer : pointers) {
        set_area (pointer, w, h);
        map (pointer, "desktop");
      }
    } break;
    case 'm': { // single monitor
      int  x, y;
      auto display = Gdk::Display::get_default ();
      display->get_default_seat ()->get_pointer ()->get_position (x, y);
      auto           mon = display->get_monitor_at_point (x, y);
      Gdk::Rectangle geo;
      mon->get_geometry (geo);
      for (const auto& pointer : pointers) {
        set_area (pointer, geo.get_width (), geo.get_height ());
        map (pointer, geo);
        // std::cout << "Tablet output set to " << mon->get_model () << '\n';
      }
    } break;
    case 'p': // precision
      if (argc > 3) {
        int  x, y;
        auto display = Gdk::Display::get_default ();
        display->get_default_seat ()->get_pointer ()->get_position (x, y);
        auto           mon = display->get_monitor_at_point (x, y);
        Gdk::Rectangle geo;
        mon->get_geometry (geo);
        // Glib::spawn_command_line_sync ("xdpyinfo", &line);
        // is.str (line.substr (line.find ("resolution:")));
        // int dpi;
        // std::getline (is, line, ':') >> dpi;
        double         multi   = argc > 4 ? std::atof (argv[4]) : 1;
        bool           rotated = geo.get_width () < geo.get_height ();
        Gdk::Rectangle area (
            0, 0,
            std::atoi (argv[2]) * geo.get_width () * multi /
                (rotated ? mon->get_height_mm () : mon->get_width_mm ()),
            std::atoi (argv[3]) * geo.get_height () * multi /
                (rotated ? mon->get_width_mm () : mon->get_height_mm ()));
        std::cout << geo.get_width () << ' ' << geo.get_height () << ' '
                  << mon->get_width_mm () << ' ' << mon->get_height_mm ()
                  << '\n';
        // int w = std::atoi (argv[2]) * dpi * c / 25.4 + .5;
        // int h = std::atoi (argv[3]) * dpi * c / 25.4 + .5;
        if (area.get_width () * geo.get_height () <
            area.get_height () * geo.get_width ()) {
          int ratioed =
              area.get_width () * geo.get_height () / geo.get_width ();
          area.set_x (geo.get_x () + (geo.get_width () - area.get_width ()) *
                                         (x - geo.get_x ()) / geo.get_width ());
          area.set_y (geo.get_y () +
                      (geo.get_height () - ratioed) * (y - geo.get_y ()) /
                          geo.get_height () -
                      (area.get_height () - ratioed) / 2);
        } else {
          int ratioed =
              area.get_height () * geo.get_width () / geo.get_height ();
          area.set_y (geo.get_y () + (geo.get_height () - area.get_height ()) *
                                         (x - geo.get_y ()) /
                                         geo.get_height ());
          area.set_x (geo.get_x () +
                      (geo.get_width () - ratioed) * (x - geo.get_x ()) /
                          geo.get_width () -
                      (area.get_width () - ratioed) / 2);
        }
        for (const auto& pointer : pointers) {
          reset_area (pointer);
          map (pointer, area);
        }
      } else {
        usage (argv[0]);
      }
      break;
    default:
      usage (argv[0]);
    }
  } else {
    usage (argv[0]);
  }
}
